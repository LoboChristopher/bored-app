const light = {
  title: 'light',

  colors: {
    primary: '#ff5b21',
    secondary: '#111820',
    white: '#FFFFFF',
    gray200: '#f5f5f5',
    
    educational: '#F3F0D7',
    recreational: '#FFE1AF',
    social: '#CEE5D0',
    diy: '#C1CFC0',
    charity: '#C9E4C5',
    cooking: '#F7D59C',
    relaxation: '#B5DEFF  ',
    music: '#B97A95',
    busywork: '#F29191',

    educationalCard: '#d7daf3',
    recreationalCard: '#AFCDFF',
    socialCard: '#e5cee3',
    diyCard: '#CEC0CF',
    charityCard: '#E0C5E4',
    cookingCard: '#9CBEF7',
    relaxationCard: '#FFD6B5',
    musicCard: '#7AB99E',
    busyworkCard: '#91F2F2',

    textDark: '#435560',
    textLight: '#FDFAF6',


  },


  fonts: {
    primary: 'Helvetica Neue, Helvetica, Arial, sans-serif',
    sizes: {
      headerLinks: '.9rem',
      headerMobileLinks: '1.2rem',
      sectionName: '.75rem',
      brandsTitle: '1.4rem',
      heroTitle: '3.5rem',
      title: '2.2rem',
      intro: '1rem',
      button: '.9rem',
      news: {
        title: '1.5rem',
        links: '.9rem',
      },
      sectionTitle: '2.5rem',
      sectionIntro: '1.25rem',
      blockTitle: '2rem',
    },
  },

  wrapper: {
    width: '1240px',
    maxWidth: '90%',
  },
}

export default light
