import 'styled-components'

declare module 'styled-components' {
  export interface DefaultTheme {
    title: string

    colors: {
      primary: string
      secondary: string
      white: string
      gray200: string

      educational: string
      recreational: string
      social: string
      diy: string
      charity: string
      cooking: string
      relaxation: string
      music: string
      busywork: string

      educationalCard: string
      recreationalCard: string
      socialCard: string
      diyCard: string
      charityCard: string
      cookingCard: string
      relaxationCard: string
      musicCard: string
      busyworkCard: string

      textDark: string
      textLight: string
    }

    fonts: {
      primary: string
      sizes: {
        headerLinks: string
        headerMobileLinks: string
        sectionName: string
        brandsTitle: string
        heroTitle: string
        title: string
        intro: string
        button: string
        news: {
          title: string
          links: string
        }
        sectionTitle: string
        sectionIntro: string
        blockTitle: string
      }
    }

    wrapper: {
      width: string
      maxWidth: string
    }
  }
}
