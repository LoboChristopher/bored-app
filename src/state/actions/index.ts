import { ActionType } from './../action-types/index';


interface Tip{
    type: ActionType.TIP,
    payload: any
}

export type Action = Tip