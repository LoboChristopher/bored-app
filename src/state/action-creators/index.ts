import { ActionType } from "../action-types/index";
import { Dispatch } from "redux";
import { Action } from "../actions/index";

export const tip = (tip: []) => {
  return (dispatch: Dispatch<Action>) => {
    dispatch({
      type: ActionType.TIP,
      payload: tip,
    });
  };
};
