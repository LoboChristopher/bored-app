import { useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";

import Cards from "../../components/Cards";
import api from "../../services/api";
import Button from "../../components/Button";
import { actionCreators, State } from "../../state";
import { Container, NextTip } from "./styles";

function Tips() {
  const dispatch = useDispatch();

  const { tip } = bindActionCreators(actionCreators, dispatch);
  const object = useSelector((state: State) => state.activity);

  async function getData() {
    try {
      const { data } = await api.get("/activity");
      tip(data);
    } catch (error) {
      alert("There was a problem fetching the activities");
    }
  }

  useEffect(() => {
    getData();
  }, []);

  const refreshData = useCallback(() => {
    getData();
  }, []);

  return (
    <Container background={object?.type}>
      {object.activity && (
        <>
          <Cards
            activity={object?.activity}
            type={object?.type}
            participants={object?.participants}
            price={object?.price}
            key={object?.key}
            link={object?.link}
            accessibility={object?.accessibility}
          />
          <NextTip>
            <span onClick={() => refreshData()}>
              <Button type={object.type} onClick={() => refreshData()}>
                Next Tip
              </Button>
            </span>
          </NextTip>
        </>
      )}
    </Container>
  );
}

export default Tips;
