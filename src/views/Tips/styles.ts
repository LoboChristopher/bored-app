import styled, { css } from "styled-components";

export const Container = styled.div<{ background: string }>`
  position: absolute;
  top: 0px;
  right: 0px;
  bottom: 0px;
  left: 0px;
  ${({ background }) =>
    background === "education" &&
    css`
      background-color: ${({ theme }) => theme.colors.educational};
    `}

  ${({ background }) =>
    background === "charity" &&
    css`
      background-color: ${({ theme }) => theme.colors.charity};
    `}

    ${({ background }) =>
    background === "recreational" &&
    css`
      background-color: ${({ theme }) => theme.colors.recreational};
    `}

    ${({ background }) =>
    background === "relaxation" &&
    css`
      background-color: ${({ theme }) => theme.colors.relaxation};
    `}

    ${({ background }) =>
    background === "busywork" &&
    css`
      background-color: ${({ theme }) => theme.colors.busywork};
    `}

    ${({ background }) =>
    background === "diy" &&
    css`
      background-color: ${({ theme }) => theme.colors.diy};
    `}

    ${({ background }) =>
    background === "social" &&
    css`
      background-color: ${({ theme }) => theme.colors.social};
    `}

    ${({ background }) =>
    background === "music" &&
    css`
      background-color: ${({ theme }) => theme.colors.music};
    `}

    ${({ background }) =>
    background === "cooking" &&
    css`
      background-color: ${({ theme }) => theme.colors.cooking};
    `}
`;

export const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Buttons = styled.button`
  display: flex;
  flex-direction: row;
`;

export const NextTip = styled.div`
  width: 50%;
  margin: 0 auto;
  margin-top: -1.5rem;
`;
