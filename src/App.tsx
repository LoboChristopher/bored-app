import Tips from "./views/Tips";
import { ThemeProvider } from "styled-components";

import lightTheme from "./styles/themes/light";

function App() {
  return (
    <ThemeProvider theme={lightTheme}>
      <Tips />
    </ThemeProvider>
  );
}

export default App;
