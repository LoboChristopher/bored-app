import React, { FC } from "react";
import { ButtonProps } from "./types";

import { Container } from "./styles";

const Button: FC<ButtonProps> = ({ children, type, theme }) => {
  return <Container>{children}</Container>;
};

export default Button;
