import styled from 'styled-components';

export const Container = styled.div`
    background-color: ${({ theme }) => theme.colors.busyworkCard};
    max-width: 100%;
    align-items: center;
    justify-content: center;
    display: flex;
    padding: 1rem;
    border-radius: 1rem;
    margin-bottom: 1rem;

    &:hover{
        cursor: pointer;
        filter: brightness(85%);
    }
    
  a{

  }
`;
