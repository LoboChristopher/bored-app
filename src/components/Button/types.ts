export interface ButtonProps extends React.HTMLAttributes<HTMLDivElement> {
    type?: 'education' | 'recreation' | 'social' | 'diy' | 'charity' | 'relaxation' | 'music' | 'busywork' | 'default'
    theme?: 'light' | 'dark'
}
