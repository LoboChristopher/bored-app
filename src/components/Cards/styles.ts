import styled, { css } from "styled-components";

export const Container = styled.div`
  min-height: 95vh;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;
`;

export const CardCointainer = styled.div<{ color: string }>`
  max-width: 31rem;
  max-height: 50rem;
  border-radius: 5rem;
  line-height: 1.75rem;
  ${({ color }) =>
    color === "education" &&
    css`
      background-color: ${({ theme }) => theme.colors.educationalCard};
      color: ${({ theme }) => theme.colors.textDark};
    `}

  ${({ color }) =>
    color === "charity" &&
    css`
      background-color: ${({ theme }) => theme.colors.charityCard};
      color: ${({ theme }) => theme.colors.textDark};
    `}

    ${({ color }) =>
    color === "recreational" &&
    css`
      background-color: ${({ theme }) => theme.colors.recreationalCard};
      color: ${({ theme }) => theme.colors.textDark};
    `}

    ${({ color }) =>
    color === "relaxation" &&
    css`
      background-color: ${({ theme }) => theme.colors.relaxationCard};
      color: ${({ theme }) => theme.colors.textDark};
    `}

    ${({ color }) =>
    color === "busywork" &&
    css`
      background-color: ${({ theme }) => theme.colors.busyworkCard};
      color: ${({ theme }) => theme.colors.textDark};
    `}

    ${({ color }) =>
    color === "diy" &&
    css`
      background-color: ${({ theme }) => theme.colors.diyCard};
      color: ${({ theme }) => theme.colors.textDark};
    `}

    ${({ color }) =>
    color === "social" &&
    css`
      background-color: ${({ theme }) => theme.colors.socialCard};
      color: ${({ theme }) => theme.colors.textDark};
    `}

    ${({ color }) =>
    color === "music" &&
    css`
      background-color: ${({ theme }) => theme.colors.musicCard};
      color: ${({ theme }) => theme.colors.textLight};
    `}

    ${({ color }) =>
    color === "cooking" &&
    css`
      background-color: ${({ theme }) => theme.colors.cookingCard};
      color: ${({ theme }) => theme.colors.textDark};
    `}
`;

export const TextContainer = styled.div`
  align-items: center;
  justify-content: center;
  display: flex;
  padding: 0 3rem 3rem 3rem;
  border-radius: 1rem;
  flex-direction: column;
  a {
    color: ${({ theme }) => theme.colors.textDark};
    text-decoration: none;
  }
`;
