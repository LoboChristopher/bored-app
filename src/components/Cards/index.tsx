import React, { FC } from "react";
import { CardTypes } from "./types";

import { Container, CardCointainer, TextContainer } from "./styles";
import Button from "../Button/index";

import Images from "../Images";

const Cards: FC<CardTypes> = ({
  activity,
  type,
  participants,
  price,
  link,
  key,
  accessibility,
}) => {
  return (
    <Container>
      <CardCointainer color={type}>
        <Images type={type} />
        <TextContainer>
          <h1>{activity}</h1>
          <h1>Type: {type}</h1>
          <h1>Participants: {participants}</h1>
          {link && (
            <a href={`${link}`} target="_blank" rel="noReferrer">
              <Button>Let me take you there.</Button>
            </a>
          )}
        </TextContainer>
      </CardCointainer>
    </Container>
  );
};

export default Cards;
