import React, { FC } from "react";
import { ImageTypes } from "./types";
import Educational from "../../assets/images/education.jpg";
import Recreational from "../../assets/images/recreational.jpg";
import Social from "../../assets/images/social.jpg";
import DIY from "../../assets/images/diy.jpg";
import Charity from "../../assets/images/charity.jpg";
import Cooking from "../../assets/images/cooking.jpg";
import Relaxation from "../../assets/images/relaxation.jpg";
import Music from "../../assets/images/music.jpg";
import Busywork from "../../assets/images/busywork.jpg";

import { Container } from "./styles";

const Images: FC<ImageTypes> = ({ type }) => {
  return (
    <>
      {type === "education" && (
        <Container>
          <img src={Educational} alt={type} />
        </Container>
      )}
      {type === "recreational" && (
        <Container>
          <img src={Recreational} alt={type} />
        </Container>
      )}
      {type === "social" && (
        <Container>
          <img src={Social} alt={type} />
        </Container>
      )}
      {type === "diy" && (
        <Container>
          <img src={DIY} alt={type} />
        </Container>
      )}
      {type === "charity" && (
        <Container>
          <img src={Charity} alt={type} />
        </Container>
      )}
      {type === "cooking" && (
        <Container>
          <img src={Cooking} alt={type} />
        </Container>
      )}
      {type === "relaxation" && (
        <Container>
          <img src={Relaxation} alt={type} />
        </Container>
      )}
      {type === "music" && (
        <Container>
          <img src={Music} alt={type} />
        </Container>
      )}
      {type === "busywork" && (
        <Container>
          <img src={Busywork} alt={type} />
        </Container>
      )}
    </>
  );
};

export default Images;
