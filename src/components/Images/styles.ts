import styled from "styled-components";

export const Container = styled.div`
  img {
    width: 100%;
    border-radius: 5rem 5rem 0 0;
    content: "";
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-image: linear-gradient(to bottom right, blue, red);
    opacity: 0.75;
  }
`;
